﻿using Eventuous;

namespace BasketballGames.Domain;

public record GameId(string Value)
	: Id(Value);