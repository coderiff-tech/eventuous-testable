﻿using static BasketballGames.Domain.GameValueObjects;

namespace BasketballGames.Domain;

public static class GameCommands
{
	public record ScheduleGame(
		GameId GameId,
		TeamId LocalTeamId, 
		TeamId VisitorTeamId, 
		DateTime StartsOn);
}