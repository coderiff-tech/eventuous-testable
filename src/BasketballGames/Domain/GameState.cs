﻿using Eventuous;

namespace BasketballGames.Domain;

public record GameState
	: State<GameState>;