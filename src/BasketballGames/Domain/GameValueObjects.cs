﻿using Eventuous;

namespace BasketballGames.Domain;

public static class GameValueObjects
{
	public record TeamId(string Value)
		: Id(Value);
}