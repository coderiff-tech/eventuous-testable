﻿using BasketballGames.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using static BasketballGames.Application.GameHttpCommands;
using static BasketballGames.Domain.GameCommands;
using static BasketballGames.Domain.GameValueObjects;

namespace BasketballGames.Application;

public static class GameCommandsApi
{
	public static WebApplication AddGameCommands(this WebApplication app)
	{
		app
			.MapCommands<GameState>()
			.MapCommand<ScheduleGameHttp, ScheduleGame>(
				"games/schedule",
				(cmd, ctx) =>
				{
					var newId = ctx.RequestServices.GetRequiredService<NewId>();
					
					var domainCommand =
						new ScheduleGame(
							new GameId(cmd.Id ?? newId()),
							new TeamId(cmd.LocalTeamId),
							new TeamId(cmd.VisitorTeamId),
							cmd.StartsOn);
					
					return domainCommand;
				});

		return app;
	}
}