﻿using BasketballGames.Domain;
using Eventuous;
using static BasketballGames.Domain.GameCommands;
// ReSharper disable ClassNeverInstantiated.Global

namespace BasketballGames.Application;

public class GameCommandService
	: CommandService<Game, GameState, GameId>
{
	public GameCommandService(
		IAggregateStore store,
		StreamNameMap streamNameMap) 
		: base(store, streamNameMap: streamNameMap)
	{
		On<ScheduleGame>()
			.InState(ExpectedState.New)
			.GetId(cmd => cmd.GameId)
			.Act((game, cmd) => game.Schedule(cmd.GameId, cmd.LocalTeamId, cmd.VisitorTeamId, cmd.StartsOn));
	}
}