﻿namespace BasketballGames.Application;

public static class GameHttpCommands
{
	public record ScheduleGameHttp(
		string? Id,
		string LocalTeamId,
		string VisitorTeamId,
		DateTime StartsOn);
}