﻿namespace Common;

public static class IdProviderFunctions
{
    public static string NewId() => Guid.NewGuid().ToString("N");
}