﻿using Eventuous;

namespace Common;

public sealed class StreamNameDescriptor
{
    private const char Separator = '-';
    public string AggregateName { get; }
    public string AggregateIdValue { get; }
    public StreamName StreamName => new($"{AggregateName}{Separator}{AggregateIdValue}");
    
    public static StreamNameDescriptor For<TId>(string aggregateName, TId id) where TId : Id 
        => new(aggregateName, id.Value);
    
    public static StreamNameDescriptor FromStreamName(StreamName streamName)
    {
        var parts = streamName.ToString().Split(new[] {Separator}, StringSplitOptions.RemoveEmptyEntries);
        if (parts.Length != 2)
        {
            throw new ArgumentException($"Unsupported stream name format: {streamName}");
        }

        return new StreamNameDescriptor(parts[0], parts[1]);
    }

    private StreamNameDescriptor(string aggregateName, string aggregateIdValue)
    {
        var isValid =
            !aggregateName.Contains(Separator)
            && !aggregateIdValue.Contains(Separator);

        if (!isValid)
        {
            throw new ArgumentException($"Separator {Separator} is a reserved char");
        }

        AggregateName = aggregateName;
        AggregateIdValue = aggregateIdValue;
    }
}