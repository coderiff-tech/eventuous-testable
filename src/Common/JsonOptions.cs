using System.Text.Json;

namespace Common;

public static class JsonOptions
{
	public static JsonSerializerOptions Options => new() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
}
