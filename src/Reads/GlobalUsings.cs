global using static Common.IdProvider;
global using static Common.IdProviderFunctions;
global using static BasketballGames.Domain.GameEvents;
global using static BasketballTeams.Domain.TeamEvents;