﻿using Eventuous.Projections.MongoDB.Tools;

namespace Reads.Scoreboard;

public record ScoreboardDocument
	: ProjectedDocument
{
	public ScoreboardDocument(string Id) 
		: base(Id)
	{
	}
}