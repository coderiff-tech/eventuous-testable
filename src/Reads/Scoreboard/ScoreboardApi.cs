﻿using Microsoft.AspNetCore.Mvc;

// ReSharper disable ConvertToPrimaryConstructor

namespace Reads.Scoreboard;

[Route("scoreboards")]
public class ScoreboardApi
	: ControllerBase
{
	private readonly QueryService<ScoreboardDocument> _scoreboardQueryService;

	public ScoreboardApi(QueryService<ScoreboardDocument> scoreboardQueryService)
		=> _scoreboardQueryService = scoreboardQueryService;
	
	[HttpGet]
	public async Task<IEnumerable<ScoreboardDocument>> GetAll(
		CancellationToken cancellationToken)
	{
		var documents = 
			await _scoreboardQueryService.Get(x => true, cancellationToken);
		return documents;
	}

	[HttpGet("{id}")]
	public async Task<ActionResult<ScoreboardDocument>> Get(string id, CancellationToken cancellationToken)
	{
		var document = await _scoreboardQueryService.GetSingleOrDefault(id, cancellationToken);
		return Ok(document);
	}
}