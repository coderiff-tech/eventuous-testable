﻿using Microsoft.AspNetCore.Mvc;

// ReSharper disable ConvertToPrimaryConstructor

namespace Reads.Team;

[Route("teams")]
public class TeamApi
	: ControllerBase
{
	private readonly QueryService<TeamDocument> _teamQueryService;

	public TeamApi(QueryService<TeamDocument> scoreboardQueryService)
		=> _teamQueryService = scoreboardQueryService;
	
	[HttpGet]
	public async Task<IEnumerable<TeamDocument>> GetAll(
		CancellationToken cancellationToken)
	{
		var documents = 
			await _teamQueryService.Get(x => true, cancellationToken);
		return documents;
	}

	[HttpGet("{id}")]
	public async Task<ActionResult<TeamDocument>> Get(string id, CancellationToken cancellationToken)
	{
		var document = await _teamQueryService.GetSingleOrDefault(id, cancellationToken);
		return Ok(document);
	}
}