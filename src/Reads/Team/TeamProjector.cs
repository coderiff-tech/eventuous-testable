﻿using Eventuous.Projections.MongoDB;
using Eventuous.Subscriptions.Context;
using MongoDB.Driver;

namespace Reads.Team;

public class TeamProjector
    : MongoProjector<TeamDocument>
{
    public TeamProjector(
        IMongoDatabase database) 
        : base(database)
    {
        On<TeamCreated>(Handler);
    }
    
    private static ValueTask<MongoProjectOperation<TeamDocument>> Handler(
        IMessageConsumeContext<TeamCreated> ctx)
    {
        var teamCreated = ctx.Message;
        var teamId = teamCreated.Id;
		
        var filter = Builders<TeamDocument>.Filter.Eq(x => x.Id, teamId);
        var document =
            new TeamDocument(teamId)
            {
                Position = ctx.GlobalPosition,
                StreamPosition = ctx.StreamPosition
            };
		
        var replaceOptions = new ReplaceOptions {IsUpsert = true};

        var operation =
            new MongoProjectOperation<TeamDocument>(async (collection, cancellationToken)
                => await collection.ReplaceOneAsync(filter, document, replaceOptions, cancellationToken));

        return ValueTask.FromResult(operation);
    }
}