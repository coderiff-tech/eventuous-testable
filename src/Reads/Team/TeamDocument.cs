﻿using Eventuous.Projections.MongoDB.Tools;

namespace Reads.Team;

public record TeamDocument
	: ProjectedDocument
{
	public TeamDocument(string Id) 
		: base(Id)
	{
	}
}