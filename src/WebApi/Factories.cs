﻿using MongoDB.Driver;

namespace WebApi;

internal static class Factories
{
    internal static class MongoClientFactory 
    {
        public static IMongoDatabase ConfigureMongo(Settings.DocumentStore documentStoreSettings) 
        {
            var mongoClientSettings = MongoClientSettings.FromConnectionString(documentStoreSettings.ConnectionString);
            var mongoClient = new MongoClient(mongoClientSettings);
            var mongoDatabase = mongoClient.GetDatabase(documentStoreSettings.DatabaseName);
            return mongoDatabase;
        }
    }
}