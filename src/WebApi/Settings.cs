﻿namespace WebApi;

internal static class Settings
{
    internal record EventStoreSubscription
    {
        public string SubscriptionId { get; init; } = string.Empty;
        public bool IsEnabled => !string.IsNullOrWhiteSpace(SubscriptionId);
        public IEnumerable<string> StreamNamePrefixes { get; init; } = Enumerable.Empty<string>();
    }
    
    internal record EventStore
    {
        public string ConnectionString { get; init; } = string.Empty;
        public string AggregateNamePrefix { get; init; } = string.Empty;
        public EventStoreSubscription CatchUpSubscription { get; init; } = new();
    }
    
    internal record DocumentStore
    {
        public string ConnectionString { get; init; } = string.Empty;
        public string DatabaseName { get; init; } = string.Empty;
    }
}