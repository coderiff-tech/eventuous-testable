﻿using Common;
using Eventuous;

namespace WebApi;

public static class Registrations
{
    public static IServiceCollection AddOpenApi(
        this IServiceCollection services)
    {
        services.AddSwaggerGen();
        return services;
    }
    
    public static IServiceCollection AddModule<TId, TState, TCommandService>(
        this IServiceCollection services,
        string aggregateName,
        StreamNameMap streamNameMap,
        string aggregateNamePrefix = "") where TId : Id
        where TState : State<TState>, new()
        where TCommandService : class, ICommandService<TState>
    {
        streamNameMap.Register<TId>(id =>
            StreamNameDescriptor.For($"{aggregateNamePrefix}{aggregateName}", id).StreamName);
        services.AddCommandService<TCommandService, TState>();
        return services;
    }
}