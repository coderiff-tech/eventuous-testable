using BasketballGames.Application;
using BasketballGames.Domain;
using BasketballTeams.Application;
using BasketballTeams.Domain;
using Common;
using EventStore.Client;
using Eventuous;
using Eventuous.EventStore;
using Eventuous.EventStore.Subscriptions;
using Eventuous.Projections.MongoDB;
using Eventuous.Subscriptions.Registrations;
using Reads;
using Reads.Scoreboard;
using Reads.Team;
using Serilog;
using WebApi;

var serilogBuilder =
	new LoggerConfiguration()
		.MinimumLevel.Verbose()
		.Enrich.FromLogContext()
		.WriteTo.Console();

Log.Logger = serilogBuilder.CreateLogger();

var builder = WebApplication.CreateBuilder(args);

try
{
	Log.Information("Starting application");
	builder.Host.UseSerilog();
	var configuration = builder.Configuration;
	var services = builder.Services;
	ConfigureServices(services, configuration);
	var app = builder.Build();
	Configure(app);
	await app.RunAsync();
}
catch (Exception exception)
{
	Log.Fatal(exception, "Application terminated unexpectedly");
}
finally
{
	await Log.CloseAndFlushAsync();
}

return;

static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
{
	TypeMap.RegisterKnownEventTypes(typeof(Game).Assembly);
	TypeMap.RegisterKnownEventTypes(typeof(Team).Assembly);
	
	var eventStoreSettings = new Settings.EventStore();
	configuration.Bind("EventStore", eventStoreSettings);
	
	var documentStoreSettings = new Settings.DocumentStore();
	configuration.Bind("DocumentStore", documentStoreSettings);

	var mongoDatabase = Factories.MongoClientFactory.ConfigureMongo(documentStoreSettings);

	var streamNameMap = new StreamNameMap();
	var aggregateNamePrefix = eventStoreSettings.AggregateNamePrefix;

	services
		.AddSingleton<IdProvider.NewId>(IdProviderFunctions.NewId)
		.AddTransient(typeof(QueryService<>))
		.AddEventStoreClient(eventStoreSettings.ConnectionString)
		.AddAggregateStore<EsdbEventStore>()
		.AddSingleton(streamNameMap)
		.AddModule<TeamId, TeamState, TeamCommandService>("team", streamNameMap, aggregateNamePrefix)
		.AddModule<GameId, GameState, GameCommandService>("game", streamNameMap, aggregateNamePrefix)
		.AddSingleton(mongoDatabase)
		.AddCheckpointStore<MongoCheckpointStore>()
		.AddOpenApi()
		.AddEndpointsApiExplorer()
		.AddControllers()
		.AddJsonOptions(options
			=> options.JsonSerializerOptions.PropertyNamingPolicy = JsonOptions.Options.PropertyNamingPolicy);

	if (eventStoreSettings.CatchUpSubscription.IsEnabled)
	{
		services
			.AddSubscription<AllStreamSubscription, AllStreamSubscriptionOptions>(
				eventStoreSettings.CatchUpSubscription.SubscriptionId,
				builder
					=>
				{
					// Projections
					builder
						.AddEventHandler<ScoreboardProjector>()
						.AddEventHandler<TeamProjector>();

					var streamPrefixes =
						eventStoreSettings.CatchUpSubscription.StreamNamePrefixes
							.Select(x => $"^{x}")
							.ToArray();

					var regExpr =
						string.IsNullOrWhiteSpace(aggregateNamePrefix)
							? string.Join("|", streamPrefixes)
							: $"^{aggregateNamePrefix}";
					
					builder
						.UseCheckpointStore<MongoCheckpointStore>()
						.Configure(cfg 
							=> cfg.EventFilter = StreamFilter.RegularExpression(regExpr))
						.WithPartitioningByStream(2);
				});
	}
}

static void Configure(WebApplication app)
{
	var assemblyPath = $"/{typeof(WebApi.Program).Assembly.GetName().Name!.ToLowerInvariant()}";

	app.AddGameCommands();
	app.AddTeamCommands();
	app.UsePathBase(assemblyPath);
	app.UseOpenApi(assemblyPath);
	app.UseHealthEndpoint();
	app.MapControllers();
}

namespace WebApi
{
	public abstract partial class Program;
}