﻿using Eventuous;

namespace BasketballTeams.Domain;

public record TeamState
	: State<TeamState>;