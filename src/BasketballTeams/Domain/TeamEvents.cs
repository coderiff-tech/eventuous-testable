﻿using Eventuous;

namespace BasketballTeams.Domain;

public static class TeamEvents
{
	[EventType("V1.TeamCreated")]
	public record TeamCreated(
		string Id,
		string Name);
}