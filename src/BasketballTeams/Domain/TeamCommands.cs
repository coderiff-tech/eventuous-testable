﻿namespace BasketballTeams.Domain;

public static class TeamCommands
{
	public record CreateTeam(
		TeamId TeamId,
		string Name);
}