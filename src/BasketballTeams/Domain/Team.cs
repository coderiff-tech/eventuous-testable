﻿using Eventuous;

namespace BasketballTeams.Domain;

public class Team
	: Aggregate<TeamState>
{
	public void Create(TeamId teamId, string name)
	{
		EnsureDoesntExist();
		var teamCreated = new TeamEvents.TeamCreated(teamId, name);
		Apply(teamCreated);
	}
}