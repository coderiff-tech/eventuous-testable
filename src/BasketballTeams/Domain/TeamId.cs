﻿using Eventuous;

namespace BasketballTeams.Domain;

public record TeamId(string Value)
	: Id(Value);