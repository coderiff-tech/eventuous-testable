﻿using BasketballTeams.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using static BasketballTeams.Application.TeamHttpCommands;
using static BasketballTeams.Domain.TeamCommands;
using Domain_TeamId = BasketballTeams.Domain.TeamId;

namespace BasketballTeams.Application;

public static class TeamCommandsApi
{
	public static WebApplication AddTeamCommands(this WebApplication app)
	{
		app
			.MapCommands<TeamState>()
			.MapCommand<CreateTeamHttp, CreateTeam>(
				"teams/create",
				(cmd, ctx) =>
				{
					var newId = ctx.RequestServices.GetRequiredService<NewId>();
					
					var domainCommand =
						new CreateTeam(
							new Domain_TeamId(cmd.Id ?? newId()),
							cmd.Name);
					
					return domainCommand;
				});

		return app;
	}
}