﻿namespace BasketballTeams.Application;

public static class TeamHttpCommands
{
	public record CreateTeamHttp(
		string? Id,
		string Name);
}