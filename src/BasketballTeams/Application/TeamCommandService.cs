﻿using BasketballTeams.Domain;
using Eventuous;
using static BasketballTeams.Domain.TeamCommands;
// ReSharper disable ClassNeverInstantiated.Global

namespace BasketballTeams.Application;

public class TeamCommandService
	: CommandService<Team, TeamState, TeamId>
{
	public TeamCommandService(
		IAggregateStore store,
		StreamNameMap streamNameMap)
		: base(store, streamNameMap: streamNameMap)
	{
		On<CreateTeam>()
			.InState(ExpectedState.New)
			.GetId(cmd => cmd.TeamId)
			.Act((team, cmd) => team.Create(cmd.TeamId, cmd.Name));
	}
}