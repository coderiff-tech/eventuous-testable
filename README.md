# eventuous-testable

DotNet application sandbox for to play with Event Sourcing and CQRS using Eventuous and some different testing approach.

It uses Event Store DB, Mongo DB, xUnit and TestContainers, amongst other libraries.

The domain is about basketball, where there are write operations regarding games, and read operations regarding scoreboard.

NOTE: It can use pre-release versions of Eventuous, as the nuget feed `https://www.myget.org/F/eventuous/api/v3/index.json` is added to *NuGet.Config*

## Requirements
- .NET 8 SDK
- Docker

## Testing Approach
Automated tests are all unit tests. No distinction is made by unit VS integration, as that categorization is not clear and deterministic enough.

Instead, the categorization is made depending on the subject under test (SUT). The following two groups seem more 
- Tests: unit tests with narrow unit, where the subject under test could be an aggregate, or a function, etc.
- EndToEndTests: unit tests with wider unit. It could be seen as an end to end test where the subject under test is the application itself, the input is a command or query and the output is the response. For example a POST or GET request which produces a response

Notice there is no use of unit vs integration categorization or similar. Although opinionated, this is a deterministic categorization that should clearly communicate the intention and goals.

IMPORTANT: Fixtures can be shared across assemblies, but collection definitions must be in the same assembly as the test that uses them.

### Tests

#### AggregateTest
This abstract class is used to implement unit tests for aggregates. This allows to test business methods in isolation without
requiring any event store or subscriptions.

#### ProjectorTest
This abstract class is used to implement unit tests for projectors and read models. This allows to test the projectors functionality
when handling a domain event, ensure idempotency, etc.

This approach uses collections to share a single database server amongst tests from different projector test classes, making
these tests quick to run by using a single real Mongo Db database server which spins up before the execution of all the tests
within the collection, and stops at the end.

In order to guarantee isolation between test executions, each test uses a unique database Id.

This is a flexible and scalable approach. More database server containers could work at the same time by creating more collections
and having each test class use the appropriate collection if needed.

### EndToEndTest
This abstract class is used to implement unit tests for vertical slices regarding functionality. 
In other words, these tests allow to test commands (i.e: write operations) and queries (i.e: read operations) 
through their http endpoints ensuring the reactivity (i.e: projections and process managers) work as expected.

This approach uses collections to share a single event store and database server amongst tests from different end to end test
classes, making these tests quick to run by using a single real Mongo Db database and Event Store Db which spin up before
the execution of all the tests within the collection, and stops at the end.

In order to guarantee isolation between test executions, each test uses a unique Id as database and stream prefix Id.
With this approach, all the streams are isolated and, for that specific test execution, the event store catch-up subscription
will ignore any other subscriptions/prefixes.

This is a flexible and scalable approach. More server containers could work at the same time by creating more collections
and having each test class use the appropriate collection if needed.

Additionally, there are some utility methods to append one or more events to a specific stream and wait for projections to finish.