﻿using Eventuous.Subscriptions;
using Eventuous.Subscriptions.Context;
using FluentAssertions;
using Reads.Scoreboard;
using TestCommon.Extensions;
using TestCommon.Fixtures;
using Xunit.Abstractions;

namespace Reads.Tests;

public static class ScoreboardProjectorTests
{
    public class Given_A_Game_Scheduled(ProjectorFixture projectorFixture, ITestOutputHelper output)
        : ProjectorTest(NewId(), projectorFixture, output)
    {
        private ScoreboardProjector _sut = null!;
        private IMessageConsumeContext _messageConsumeContext = null!;
        private EventHandlingStatus _result = EventHandlingStatus.Pending;
        private string _id = null!;
        private ScoreboardDocument _expectedDocument = null!;

        protected override Task Given()
        {
            _sut = new ScoreboardProjector(Database);
    
            _id = 1.ToIdAsGuidOnlyDigits();
    
            var domainEvent = 
                new GameScheduled(
                    _id, 
                    11.ToIdAsGuidOnlyDigits(), 
                    12.ToIdAsGuidOnlyDigits(),
                    1.ToUtcDate());
    
            _messageConsumeContext = domainEvent.AsMessageConsumeContext();
    
            _expectedDocument = new ScoreboardDocument(_id);
    
            return Task.CompletedTask;
        }

        protected override async Task When()
            => _result = await _sut.HandleEvent(_messageConsumeContext);

        [Fact]
        public void It_Should_Succeed_Handling()
            => _result.Should().Be(EventHandlingStatus.Success);

        [Fact]
        public Task It_Should_Project_The_Expected_Document()
            => Database.ShouldHaveExpectedDocument(_id, _expectedDocument);
    }
    
    
    public class Given_A_Game_Scheduled_Is_Handled_Twice(ProjectorFixture projectorFixture, ITestOutputHelper output)
        : ProjectorTest(NewId(), projectorFixture, output)
    {
        private ScoreboardProjector _sut = null!;
        private IMessageConsumeContext _messageConsumeContext = null!;
        private EventHandlingStatus _result = EventHandlingStatus.Pending;
        private string _id = null!;
        private ScoreboardDocument _expectedDocument = null!;

        protected override async Task Given()
        {
            _sut = new ScoreboardProjector(Database);
    
            _id = 1.ToIdAsGuidOnlyDigits();
    
            var domainEvent = 
                new GameScheduled(
                    _id, 
                    11.ToIdAsGuidOnlyDigits(), 
                    12.ToIdAsGuidOnlyDigits(),
                    1.ToUtcDate());
    
            _messageConsumeContext = domainEvent.AsMessageConsumeContext();
            
            // Handle the first time
            await _sut.HandleEvent(_messageConsumeContext);
    
            _expectedDocument = new ScoreboardDocument(_id);
        }

        protected override async Task When()
            => _result = await _sut.HandleEvent(_messageConsumeContext);

        [Fact]
        public void It_Should_Succeed_Handling()
            => _result.Should().Be(EventHandlingStatus.Success);

        [Fact]
        public Task It_Should_Project_The_Expected_Document()
            => Database.ShouldHaveExpectedDocument(_id, _expectedDocument);

        [Fact]
        public Task It_Should_Only_Project_One_Document()
            => Database.ShouldHaveExpectedDocumentCount<ScoreboardDocument>(1);
    }
}