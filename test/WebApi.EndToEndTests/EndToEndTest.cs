﻿using TestCommon;
using TestCommon.Fixtures;
using Xunit.Abstractions;

namespace WebApi.EndToEndTests;

[CollectionDefinition("EndToEnd")]
public class SharedDatabase
    : ICollectionFixture<EndToEndFixture>
{
    // This class has no code, and is never created. Its purpose is simply
    // to be the place to apply [CollectionDefinition] and all the
    // ICollectionFixture<> interfaces.
    // As per https://xunit.net/docs/shared-context
    // Fixtures can be shared across assemblies, but collection definitions must be in the same assembly as the test that uses them.
}

[Collection("EndToEnd")]
public abstract class EndToEndTest(string testId, EndToEndFixture endToEndFixture, ITestOutputHelper output)
    : EndToEndTestBase<Program>(testId, endToEndFixture, output);