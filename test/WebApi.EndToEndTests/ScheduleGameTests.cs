﻿using System.Net;
using System.Net.Http.Json;
using FluentAssertions;
using TestCommon.Fixtures;
using Xunit.Abstractions;

namespace WebApi.EndToEndTests;

public static class ScheduleGameTests
{
    private static string BaseUrl => "games/schedule";
    
    public class Given_No_Game(EndToEndFixture endToEndFixture, ITestOutputHelper output)
        : EndToEndTest(NewId(), endToEndFixture, output)
    {
        private ScheduleGameHttp _command = null!;
        private HttpResponseMessage  _result = null!;

        protected override Task Given()
        {
            _command = 
                new ScheduleGameHttp(
                    1.ToIdAsGuidOnlyDigits(),
                    "local", 
                    "visitor",
                new DateTime(2024, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            return Task.CompletedTask;
        }

        protected override async Task When()
            => _result = await HttpClient.PostAsJsonAsync(BaseUrl, _command);

        [Fact]
        public void It_Should_Return_200_Ok()
            => _result.StatusCode.Should().Be(HttpStatusCode.OK);
    }
}