global using Xunit;
global using static Common.IdProviderFunctions;
global using static BasketballGames.Application.GameHttpCommands;
global using static TestCommon.Extensions.IntExtensions;
global using static BasketballGames.Domain.GameEvents;