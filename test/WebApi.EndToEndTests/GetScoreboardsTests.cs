﻿using System.Net;
using BasketballGames.Domain;
using FluentAssertions;
using Reads.Scoreboard;
using TestCommon.Extensions;
using TestCommon.Fixtures;
using Xunit.Abstractions;

namespace WebApi.EndToEndTests;

public static class GetScoreboardsTests
{
    private static string BaseUrl => "/scoreboards";
    
    public class Given_No_Game(EndToEndFixture endToEndFixture, ITestOutputHelper output)
        : EndToEndTest(NewId(), endToEndFixture, output)
    {
        private HttpResponseMessage _result = null!;
        private IEnumerable<ScoreboardDocument> _expectedResult = null!;

        protected override Task Given()
        {
            _expectedResult = [];
            return Task.CompletedTask;
        }

        protected override async Task When()
            => _result = await HttpClient.GetAsync(BaseUrl);

        [Fact]
        public void It_Should_Return_200_Ok()
            => _result.StatusCode.Should().Be(HttpStatusCode.OK);

        [Fact]
        public async Task It_Should_Be_The_Expected_Empty_Result()
            => await _result.ShouldBeExpectedDocuments(_expectedResult);
    }
    
    public class Given_Three_Games(EndToEndFixture endToEndFixture, ITestOutputHelper output)
        : EndToEndTest(NewId(), endToEndFixture, output)
    {
        private HttpResponseMessage _result = null!;
        private IEnumerable<ScoreboardDocument> _expectedResult = null!;

        protected override async Task Given()
        {
            var gameIdOne = new GameId(1.ToIdAsGuidOnlyDigits());
            var gameScheduledOne =
                new GameScheduled(gameIdOne.Value, 11.ToIdAsGuidOnlyDigits(), 12.ToIdAsGuidOnlyDigits(), 1.ToUtcDate());
            var appendResultOne = await AppendEvent(gameIdOne, gameScheduledOne);
                
            var gameIdTwo = new GameId(2.ToIdAsGuidOnlyDigits());
            var gameScheduledTwo =
                new GameScheduled(gameIdTwo.Value, 13.ToIdAsGuidOnlyDigits(), 14.ToIdAsGuidOnlyDigits(), 1.ToUtcDate());
            var appendResultTwo = await AppendEvent(gameIdTwo, gameScheduledTwo);
            
            var gameIdThree = new GameId(3.ToIdAsGuidOnlyDigits());
            var gameScheduledThree =
                new GameScheduled(gameIdThree.Value, 15.ToIdAsGuidOnlyDigits(), 16.ToIdAsGuidOnlyDigits(), 1.ToUtcDate());
            var appendResultThree = await AppendEvent(gameIdThree, gameScheduledThree);

            await WaitForPosition(appendResultThree.GlobalPosition);

            _expectedResult = [
                new ScoreboardDocument(gameIdOne){Position = appendResultOne.GlobalPosition, StreamPosition = (ulong) appendResultOne.NextExpectedVersion},
                new ScoreboardDocument(gameIdTwo){Position = appendResultTwo.GlobalPosition, StreamPosition = (ulong) appendResultTwo.NextExpectedVersion},
                new ScoreboardDocument(gameIdThree){Position = appendResultThree.GlobalPosition, StreamPosition = (ulong) appendResultThree.NextExpectedVersion}
            ];
        }

        protected override async Task When()
            => _result = await HttpClient.GetAsync(BaseUrl);

        [Fact]
        public void It_Should_Return_200_Ok()
            => _result.StatusCode.Should().Be(HttpStatusCode.OK);

        [Fact]
        public async Task It_Should_Be_The_Expected_Documents()
            => await _result.ShouldBeExpectedDocuments(_expectedResult);
    }
    
    public class Given_Three_Games_And_Deleting_Read_Models(EndToEndFixture endToEndFixture, ITestOutputHelper output)
        : EndToEndTest(NewId(), endToEndFixture, output)
    {
        private HttpResponseMessage _result = null!;
        private IEnumerable<ScoreboardDocument> _expectedResult = null!;

        protected override async Task Given()
        {
            var gameIdOne = new GameId(1.ToIdAsGuidOnlyDigits());
            var gameScheduledOne =
                new GameScheduled(gameIdOne.Value, 11.ToIdAsGuidOnlyDigits(), 12.ToIdAsGuidOnlyDigits(), 1.ToUtcDate());
            var appendResultOne = await AppendEvent(gameIdOne, gameScheduledOne);
                
            var gameIdTwo = new GameId(2.ToIdAsGuidOnlyDigits());
            var gameScheduledTwo =
                new GameScheduled(gameIdTwo.Value, 13.ToIdAsGuidOnlyDigits(), 14.ToIdAsGuidOnlyDigits(), 1.ToUtcDate());
            var appendResultTwo = await AppendEvent(gameIdTwo, gameScheduledTwo);
            
            var gameIdThree = new GameId(3.ToIdAsGuidOnlyDigits());
            var gameScheduledThree =
                new GameScheduled(gameIdThree.Value, 15.ToIdAsGuidOnlyDigits(), 16.ToIdAsGuidOnlyDigits(), 1.ToUtcDate());
            var appendResultThree = await AppendEvent(gameIdThree, gameScheduledThree);

            await WaitForPosition(appendResultThree.GlobalPosition);

            _expectedResult = [
                new ScoreboardDocument(gameIdOne){Position = appendResultOne.GlobalPosition, StreamPosition = (ulong) appendResultOne.NextExpectedVersion},
                new ScoreboardDocument(gameIdTwo){Position = appendResultTwo.GlobalPosition, StreamPosition = (ulong) appendResultTwo.NextExpectedVersion},
                new ScoreboardDocument(gameIdThree){Position = appendResultThree.GlobalPosition, StreamPosition = (ulong) appendResultThree.NextExpectedVersion}
            ];
        }

        protected override async Task When()
            => _result = await HttpClient.GetAsync(BaseUrl);

        [Fact]
        public void It_Should_Return_200_Ok()
            => _result.StatusCode.Should().Be(HttpStatusCode.OK);

        [Fact]
        public async Task It_Should_Be_The_Expected_Documents()
            => await _result.ShouldBeExpectedDocuments(_expectedResult);
    }
}