﻿using System.Net;
using FluentAssertions;
using TestCommon.Fixtures;
using Xunit.Abstractions;

namespace WebApi.EndToEndTests;

public static class HealthCheckTests
{
    public class Given_Health_Check_Url(EndToEndFixture endToEndFixture, ITestOutputHelper output)
        : EndToEndTest(NewId(), endToEndFixture, output)
    {
        private string _url = null!;
        private HttpResponseMessage _result = null!;

        protected override Task Given()
        {
            _url = "/health";
            return Task.CompletedTask;
        }

        protected override async Task When()
            => _result = await HttpClient.GetAsync(_url);

        [Fact]
        public void It_Should_Return_200_Ok()
            => _result.StatusCode.Should().Be(HttpStatusCode.OK);
    }
}