﻿using BasketballGames.Domain;
using Eventuous;
using TestCommon;
using TestCommon.Extensions;

namespace BasketballGame.Tests;

public static class ScheduleGameTests
{
	public class Given_No_Game_And_A_Command_To_Schedule_One
		: AggregateTest<Game, GameState>
	{
		protected override IEnumerable<object> GivenEvents()
			=> [];

		protected override Task When(Game aggregate)
		{
			aggregate
				.Schedule(
					new GameId(1.ToIdAsGuidOnlyDigits()),
					new TeamId("one"),
					new TeamId("two"),
					1.ToUtcDate());

			return Task.CompletedTask;
		}

		[Fact]
		public async Task It_Should_Produce_Game_Scheduled()
		{
			await Sut.ShouldContainEvent(new GameScheduled(1.ToIdAsGuidOnlyDigits(), "one", "two", 1.ToUtcDate()));
		}
	}
	
	public class Existing_Game_And_A_Command_To_Schedule_One_With_The_Same_Id
		: AggregateTest<Game, GameState>
	{
		protected override IEnumerable<object> GivenEvents()
			=> [new GameScheduled(1.ToIdAsGuidOnlyDigits(), "foo", "bar", new DateTime())];

		protected override Task When(Game aggregate)
		{
			aggregate
				.Schedule(
					new GameId(1.ToIdAsGuidOnlyDigits()),
					new TeamId("one"),
					new TeamId("two"),
					1.ToUtcDate());

			return Task.CompletedTask;
		}

		[Fact]
		public async Task It_Should_Throw_A_Domain_Exception()
		{
			await Sut.ShouldThrow<DomainException>();
		}
	}
}