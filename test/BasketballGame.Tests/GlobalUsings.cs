global using Xunit;
global using static Common.IdProviderFunctions;
global using static BasketballGames.Domain.GameValueObjects;
global using static BasketballGames.Domain.GameEvents;