﻿using BasketballTeams.Domain;
using Eventuous;
using TestCommon;
using TestCommon.Extensions;

namespace BasketballTeam.Tests;

public static class CreateTeamTests
{
	public class Given_No_Team_And_A_Command_To_Create_One
		: AggregateTest<Team, TeamState>
	{
		protected override IEnumerable<object> GivenEvents()
			=> [];

		protected override Task When(Team aggregate)
		{
			aggregate
				.Create(
				new TeamId(1.ToIdAsGuidOnlyDigits()),
				"foo");

			return Task.CompletedTask;
		}

		[Fact]
		public async Task It_Should_Produce_Team_Created()
			=> await Sut.ShouldContainEvent(new TeamCreated(1.ToIdAsGuidOnlyDigits(), "foo"));
	}
	
	public class Given_Existing_Team_And_A_Command_To_Create_One_With_The_Same_Id
		: AggregateTest<Team, TeamState>
	{
		protected override IEnumerable<object> GivenEvents()
			=> [new TeamCreated(1.ToIdAsGuidOnlyDigits(), "foo")];

		protected override Task When(Team aggregate)
		{
			aggregate
				.Create(
					new TeamId(1.ToIdAsGuidOnlyDigits()),
					"bar");

			return Task.CompletedTask;
		}

		[Fact]
		public async Task It_Should_Throw_A_Domain_Exception()
			=> await Sut.ShouldThrow<DomainException>();
	}
}