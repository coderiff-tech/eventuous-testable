﻿using Xunit;

namespace TestCommon;

public abstract class GivenWhenThen
    : IAsyncLifetime
{
    public async Task InitializeAsync()
    {
        await Given();
        await When();
    }

    public Task DisposeAsync() => Cleanup();

    protected abstract Task Given();

    protected abstract Task When();

    protected virtual Task Cleanup()
    {
        return Task.CompletedTask;
    }
}
