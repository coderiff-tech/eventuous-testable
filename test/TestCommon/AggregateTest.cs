﻿using Eventuous;
using FluentAssertions;
using Xunit;
// ReSharper disable ConvertToPrimaryConstructor

namespace TestCommon;

public abstract class AggregateTest<TAggregate, TState>
	: IAsyncLifetime
	where TAggregate : Aggregate<TState>
	where TState: State<TState>, new()
{
	public Task InitializeAsync()
	{
		return Task.CompletedTask;
	}

	public async Task DisposeAsync() => await Cleanup();

	protected abstract IEnumerable<object> GivenEvents();

	protected abstract Task When(TAggregate aggregate);

	internal async Task<TAggregate> Then()
	{
		var instance = AggregateFactoryRegistry.Instance.CreateInstance<TAggregate, TState>();
		instance.Load(GivenEvents());
		await When(instance);

		return instance;
	}

	protected virtual Task Cleanup()
	{
		return Task.CompletedTask;
	}

	protected AggregateWrapper<TAggregate, TState> Sut => new(this);
}

public class AggregateWrapper<TAggregate, TAggregateState>
	where TAggregate : Aggregate<TAggregateState>
	where TAggregateState: State<TAggregateState>, new()
{
	private readonly AggregateTest<TAggregate, TAggregateState> _aggregateTest;

	public AggregateWrapper(AggregateTest<TAggregate, TAggregateState> aggregateTest)
		=> _aggregateTest = aggregateTest;

	public async Task ShouldThrow<TException>()
		where TException : Exception
	{
		Func<Task> act = () => _aggregateTest.Then();
		await act.Should().ThrowAsync<TException>();
	}

	public async Task ShouldContainEvent<TEvent>(TEvent domainEvent)
		where TEvent : class
	{
		var aggregate = await _aggregateTest.Then();
		aggregate.Changes.Should().Contain(domainEvent);
	}

	public async Task ShouldHaveState(TAggregateState state) {
		var aggregate = await _aggregateTest.Then();
		aggregate.State.Should().BeEquivalentTo(state);
	}
}