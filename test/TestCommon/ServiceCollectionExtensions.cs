﻿using Microsoft.Extensions.DependencyInjection;

namespace TestCommon;

public static class ServiceCollectionExtensions
{
    public static bool RemoveRegisteredService<TService>(this IServiceCollection services)
    {
        var serviceDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(TService));
        return serviceDescriptor is not null && services.Remove(serviceDescriptor);
    }
}