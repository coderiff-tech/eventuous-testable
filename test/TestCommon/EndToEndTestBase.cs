﻿using EventStore.Client;
using Eventuous;
using Eventuous.EventStore;
using Eventuous.Subscriptions.Checkpoints;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using TestCommon.Fixtures;
using Xunit.Abstractions;

namespace TestCommon;

public abstract class EndToEndTestBase<TEntryPoint>
    : GivenWhenThen
    where TEntryPoint : class
{
    protected HttpClient HttpClient { get; }
    
    private string TestId { get; }
    private ITestOutputHelper Output { get; }
    private readonly IServiceProvider _services;


    protected EndToEndTestBase(string testId, EndToEndFixture endToEndFixture, ITestOutputHelper output)
    {
        TestId = testId;
        Output = output;

        var databaseConnectionString = endToEndFixture.DatabaseConnectionString;
        var mongoDbSettings = MongoClientSettings.FromConnectionString(databaseConnectionString);
        var database = new MongoClient(mongoDbSettings).GetDatabase(testId);

        var eventStoreConnectionString = endToEndFixture.EventStoreConnectionString;

        var configurationBuilder =
            new ConfigurationBuilder()
                .AddInMemoryCollection(
                    new List<KeyValuePair<string, string?>>
                    {
                        new("EventStore:AggregateNamePrefix", testId),
                        new("EventStore:CatchUpSubscription:SubscriptionId", testId)
                    })
                .Build();

        var webApplicationFactory =
            new WebApplicationFactory<TEntryPoint>()
                .WithWebHostBuilder(webHostBuilder =>
                    webHostBuilder
                        .UseEnvironment("Development")
                        .UseConfiguration(configurationBuilder)
                        .ConfigureServices(services =>
                        {
                            services.RemoveRegisteredService<EventStoreClient>();
                            services.AddEventStoreClient(eventStoreConnectionString);

                            services.RemoveRegisteredService<IMongoDatabase>();
                            services.AddSingleton(database);
                        }));

        var httpClientOptions =
            new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            };

        HttpClient = webApplicationFactory.CreateClient(httpClientOptions);

        _services = webApplicationFactory.Services;

        output.WriteLine($"Running test {testId} on MongoDb server {databaseConnectionString} " +
                         $"and on EventStoreDb server {eventStoreConnectionString}");
    }

    protected Task<AppendEventsResult> AppendEvent<TId>(
        TId aggregateId,
        object domainEvent,
        ExpectedStreamVersion? version = null)
        where TId : Id
        => AppendEventsInternal(aggregateId, new[] { domainEvent }, version);

    protected Task<AppendEventsResult> AppendEvents<TId>(
        TId aggregateId,
        IEnumerable<object> domainEvents,
        ExpectedStreamVersion? version = null)
        where TId : Id
        => AppendEventsInternal(aggregateId, domainEvents, version);

    protected Task DeleteDatabase()
    {
        return Task.CompletedTask;
    }

    protected async Task WaitForPosition(ulong position)
    {
        var checkpointStore = (_services ?? throw new InvalidOperationException()).GetRequiredService<ICheckpointStore>();
        var count = 100;

        while (count-- > 0) 
        {
            var checkpoint = await checkpointStore.GetLastCheckpoint(TestId, default);

            if (checkpoint.Position.HasValue && checkpoint.Position.Value >= position)
            {
                Output.WriteLine($"Position {position} reached in checkpoint");
                break;
            }

            await Task.Delay(100);
        }
    }
    
    private StreamName GetStreamName<TId>(TId aggregateId)
        where TId: Id
    {
        var streamNameMap = _services.GetRequiredService<StreamNameMap>();
        return streamNameMap.GetStreamName(aggregateId);
    }

    private Task<AppendEventsResult> AppendEventsInternal<TId>(
        TId aggregateId,
        IEnumerable<object> domainEvents,
        ExpectedStreamVersion? version = null)
        where TId : Id
    {
        var eventStore = _services.GetRequiredService<IEventStore>();
        return eventStore
            .AppendEvents(
                GetStreamName(aggregateId),
                version ?? ExpectedStreamVersion.Any,
                domainEvents.Select(CreateStreamEvent).ToList(),
                CancellationToken.None
            );
    } 
    
    private static StreamEvent CreateStreamEvent(object domainEvent) 
        => new(Guid.NewGuid(), domainEvent, new Metadata(), "application/json", 0);
}