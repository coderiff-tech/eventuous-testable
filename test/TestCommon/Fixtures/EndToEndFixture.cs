﻿using Xunit;
using static TestCommon.ServerContainers;
// ReSharper disable ClassNeverInstantiated.Global

namespace TestCommon.Fixtures;

public class EndToEndFixture
    : IAsyncLifetime
{
    private EventStoreServerContainer _eventStoreDbContainer = null!;
    private MongoServerContainer _mongoServerContainer = null!;
    
    public string EventStoreConnectionString { get; private set; } = null!;
    public string DatabaseConnectionString { get; private set; } = null!;
    
    public async Task InitializeAsync()
    {
        _eventStoreDbContainer = new EventStoreServerContainer();
        await _eventStoreDbContainer.Start();
        EventStoreConnectionString = _eventStoreDbContainer.ConnectionString;
        
        _mongoServerContainer = new MongoServerContainer();
        await _mongoServerContainer.Start();
        DatabaseConnectionString = _mongoServerContainer.ConnectionString;
    }

    public Task DisposeAsync() => Task.WhenAll(_mongoServerContainer.Stop(), _eventStoreDbContainer.Stop());
}