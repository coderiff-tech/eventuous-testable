﻿using Xunit;
using static TestCommon.ServerContainers;
// ReSharper disable ClassNeverInstantiated.Global

namespace TestCommon.Fixtures;

public class ProjectorFixture
    : IAsyncLifetime
{
    private MongoServerContainer _mongoServerContainer = null!;
    public string DatabaseConnectionString { get; private set; } = null!;
    
    public async Task InitializeAsync()
    {
        _mongoServerContainer = new MongoServerContainer();
        await _mongoServerContainer.Start();
        DatabaseConnectionString = _mongoServerContainer.ConnectionString;
    }

    public Task DisposeAsync() => _mongoServerContainer.Stop();
}