﻿using Testcontainers.EventStoreDb;
using Testcontainers.MongoDb;
// ReSharper disable ConvertConstructorToMemberInitializers
// ReSharper disable ConvertToPrimaryConstructor
// ReSharper disable StringLiteralTypo

namespace TestCommon;

public static class ServerContainers
{
    private interface IServerContainer
    {
        string ConnectionString { get; }
        Task Start();
        Task Stop();
    }
    
    private class ServerContainerNotInitializedException
        : Exception
    {
        public ServerContainerNotInitializedException(string serverName)
            : base($"The server {serverName} has not been initialized")
        {
        }
    }
    
    public class MongoServerContainer
        : IServerContainer
    {
        private readonly MongoDbContainer _mongoDbContainer;
        private bool _isStarted;
        
        public MongoServerContainer()
        {
            _mongoDbContainer =
                new MongoDbBuilder()
                    .WithImage("mongo:7.0.8-jammy")
                    .Build();

            _isStarted = false;
        }

        public string ConnectionString => !_isStarted
            ? throw new ServerContainerNotInitializedException(nameof(MongoServerContainer))
            : _mongoDbContainer.GetConnectionString();
        
        public async Task Start()
        {
            if (_isStarted)
            {
                return;
            }
            
            await _mongoDbContainer.StartAsync();
            _isStarted = true;
        }

        public async Task Stop()
        {
            await _mongoDbContainer.StopAsync();
            await _mongoDbContainer.DisposeAsync();
        }
    }
    
    public class EventStoreServerContainer
        : IServerContainer
    {
        private readonly EventStoreDbContainer _eventStoreDbContainer;
        private bool _isStarted;
        
        public EventStoreServerContainer()
        {
            _eventStoreDbContainer =
                new EventStoreDbBuilder()
                    .WithImage("eventstore/eventstore:24.2.0-jammy")
                    .WithEnvironment("EVENTSTORE_ENABLE_ATOM_PUB_OVER_HTTP", "true")
                    .WithEnvironment("EVENTSTORE_RUN_PROJECTIONS", "None")
                    .Build();

            _isStarted = false;
        }

        public string ConnectionString => !_isStarted
            ? throw new ServerContainerNotInitializedException(nameof(MongoServerContainer))
            : _eventStoreDbContainer.GetConnectionString();
        
        public async Task Start()
        {
            if (_isStarted)
            {
                return;
            }
            
            await _eventStoreDbContainer.StartAsync();
            _isStarted = true;
        }

        public async Task Stop()
        {
            await _eventStoreDbContainer.StopAsync();
            await _eventStoreDbContainer.DisposeAsync();
        }
    }
}