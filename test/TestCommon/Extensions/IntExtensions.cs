﻿namespace TestCommon.Extensions;

public static class IntExtensions
{
	public static string ToIdAsGuidOnlyDigits(this int value) => value.ToString("D32");

	public static DateTime ToUtcDate(this int value) 
		=> new DateTime(2024, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddHours(value);
}
