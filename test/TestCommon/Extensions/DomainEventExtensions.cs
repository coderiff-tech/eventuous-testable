﻿using Eventuous.Subscriptions.Context;

namespace TestCommon.Extensions;

public static class DomainEventExtensions
{
    public static IMessageConsumeContext AsMessageConsumeContext(
        this object domainEvent,
        Guid? eventId = null,
        string? eventType = null,
        string? streamName = null,
        int? eventNumber = null,
        int? streamPosition = null,
        int? globalPosition = null,
        DateTime? createdOn = null,
        string? subscriptionId = null,
        CancellationToken? cancellationToken = null)
    {
        var messageConsumeContext =
            new MessageConsumeContext(
                eventId?.ToString() ?? Guid.NewGuid().ToString(), 
                eventType ?? "Irrelevant", 
                "application/json", 
                streamName ?? "Irrelevant", 
                (ulong)(eventNumber ?? 0), 
                (ulong)(streamPosition ?? 0), 
                (ulong)(globalPosition ?? 0), 
                1,
                createdOn ?? DateTime.UtcNow,
                domainEvent, 
                null, 
                subscriptionId ?? "Irrelevant",
                cancellationToken ?? new CancellationToken());

        return messageConsumeContext;
    }
}