using System.Text.Json;
using Common;
using Eventuous.Projections.MongoDB.Tools;
using FluentAssertions;

namespace TestCommon.Extensions;

public static class HttpResponseMessageExtensions
{
	public static async Task ShouldBeExpectedDocument<T>(this HttpResponseMessage response, T expectedDocument)
		where T: ProjectedDocument
	{
		var document = await response.ToPayload<T>();
		document.Should().BeEquivalentTo(expectedDocument);
	}
	
	public static async Task ShouldBeExpectedDocuments<T>(this HttpResponseMessage response, IEnumerable<T> expectedDocuments)
		where T: ProjectedDocument
	{
		var document = await response.ToPayload<IEnumerable<T>>();
		document.Should().BeEquivalentTo(expectedDocuments);
	}

	private static async Task<T> ToPayload<T>(this HttpResponseMessage response)
	{
		var content = await response.Content.ReadAsStringAsync();
		var result = Deserialize<T>(content);
		return result;
	}

	private static async Task<object> ToPayload(this HttpResponseMessage response, object anonymousTypeInstance)
	{
		var content = await response.Content.ReadAsStringAsync();
		var result = Deserialize(content, anonymousTypeInstance.GetType());
		return result;
	}

	private static T Deserialize<T>(string json)
	{
		return (T) Deserialize(json, typeof(T));
	}

	private static object Deserialize(string json, Type destinationType)
	{
		try
		{
			var result =
				JsonSerializer.Deserialize(json, destinationType, JsonOptions.Options)
				?? throw new Exception($"Unable to deserialize json {json} into {destinationType}");
			return result;
		}
		catch (Exception exception)
		{
			throw new Exception($"Unable to deserialize json {json} into {destinationType}", exception);
		}
	}
}
