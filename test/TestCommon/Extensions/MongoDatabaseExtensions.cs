﻿using Eventuous.Projections.MongoDB.Tools;
using FluentAssertions;
using MongoDB.Driver;

namespace TestCommon.Extensions;

public static class MongoDatabaseExtensions
{
    public static async Task ShouldHaveExpectedDocument<T>(
        this IMongoDatabase mongoDatabase, 
        string documentId, 
        T expectedDocument)
        where T: ProjectedDocument
    {
        var document = await mongoDatabase.GetDocumentCollection<T>().LoadDocument(documentId);
        document.Should().BeEquivalentTo(expectedDocument);
    }
    
    public static async Task ShouldHaveExpectedDocuments<T>(
        this IMongoDatabase mongoDatabase,
        IEnumerable<T> expectedDocuments)
        where T: ProjectedDocument
    {
        var documentsCursor = await mongoDatabase.GetDocumentCollection<T>().FindAsync(x => true);
        var documents = await documentsCursor.ToListAsync();
        documents.Should().BeEquivalentTo(expectedDocuments);
    }
    
    public static async Task ShouldHaveExpectedDocumentCount<T>(
        this IMongoDatabase mongoDatabase,
        int expectedCount)
        where T: ProjectedDocument
    {
        var count = await mongoDatabase.GetDocumentCollection<T>().CountDocumentsAsync(x => true);
        count.Should().Be(expectedCount);
    }
}