﻿using MongoDB.Driver;
using TestCommon.Fixtures;
using Xunit.Abstractions;

namespace TestCommon;

public abstract class ProjectorTestBase
    : GivenWhenThen
{
    protected IMongoDatabase Database { get; }

    protected ProjectorTestBase(string testId, ProjectorFixture projectorFixture, ITestOutputHelper output)
    {
        var connectionString = projectorFixture.DatabaseConnectionString;
        var mongoDbSettings = MongoClientSettings.FromConnectionString(connectionString);
        Database = new MongoClient(mongoDbSettings).GetDatabase(testId);
        output.WriteLine($"Running test {testId} on MongoDb server {connectionString}");
    }
}